# This is an example PKGBUILD file. Use this as a start to creating your own,
# and remove these comments. For more information, see 'man PKGBUILD'.
# NOTE: Please fill out the license field for your package! If it is unknown,
# then please put 'unknown'.

# Maintainer: Future Linux Team <future_linux@163.com>
pkgname=filesystem
pkgver=2024.01.14
pkgrel=1
pkgdesc="Base Future Linux files"
arch=('x86_64')
url="https://futurelinux.xyz/"
license=('GPL-3.0-or-later')
groups=('base')
depends=('iana-etc')
backup=(etc/fstab
    etc/group
    etc/hostname
    etc/hosts
    etc/issue
    etc/locale.conf
    etc/os-release
    etc/passwd)
source=(btmp
    faillog
    fstab
    futurelinux-logo.png
    futurelinux-logo.svg
    futurelinux-logo-text-dark.svg
    futurelinux-logo-text.svg
    future-release
    group
    hostname
    hosts
    issue
    lastlog
    locale.conf
    os-release
    passwd
    wtmp)
sha256sums=(e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855
    e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855
    45d48dd125685c737136fc3deae94887bbdba46a5fb59244fb1ffb39cadb6620
    e25f17bbbf6be0c95f36789e25b6d8f1d29da2df71794ec8ad2c4d6c2e14fd1e
    cdd69f562b4ef6f341745824a3d36a05ce01096e37e03489ee8861171c9f97b3
    b6d71ac001f5e4c6b440a09355436b2eab6aeeadc3fb757ae8bcd8b4cd863563
    89f84c5c4eece74b37d5cd0d45fde3bb1ed634f53d03a2e5c0faac48e15ef3fc
    e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855
    005ef4e65e0f761e5d0f4b707c7f288e2492681cfe9835ff8caa8edfc33c1bc2
    26520c981703c1ddc64c417d40861ec8e609df0db3de89a839fdf5f7aca87c03
    13cb6513d6484f27053868f8f8f106b50386e406c7fd75fa98a7642ecd69c5dd
    373af47d05b3b895092f43ed895d4680e0127627c05f2187d56794d2f1a43a57
    e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855
    0328593405c8c79ba4677a7d227cff0af65fcf4e6fde227bd80762900bf2cd80
    ce9a337dda498168a2e372402fcebe090ba9a0908c5073d22622c945d5245635
    7817ee628fccd203d7f1bcc20f0afbbaaae615b5bc900314995da35764df0b47
    e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855)

package() {
    local group link mode source_file user
    declare -A directories
    declare -A files
    declare -A symlinks

    directories=(
        ["boot"]="755:0:0"
        ["dev"]="755:0:0"
        ["etc/opt"]="755:0:0"
        ["etc/sysconfig"]="755:0:0"
        ["home"]="755:0:0"
        ["media/floppy"]="755:0:0"
        ["media/cdrom"]="755:0:0"
        ["mnt"]="755:0:0"
        ["opt"]="755:0:0"
        ["proc"]="555:0:0"
        ["root"]="0750:0:0"
        ["run"]="1777:0:0"
        ["srv"]="755:0:0"
        ["sys"]="555:0:0"
        ["tmp"]="1777:0:0"
        ["usr/bin"]="755:0:0"
        ["usr/include"]="755:0:0"
        ["usr/lib"]="755:0:0"
        ["usr/lib/firmware"]="755:0:0"
        ["usr/lib64"]="755:0:0"
        ["usr/sbin"]="755:0:0"
        ["usr/share/color"]="755:0:0"
        ["usr/share/dict"]="755:0:0"
        ["usr/share/doc"]="755:0:0"
        ["usr/share/info"]="755:0:0"
        ["usr/share/locale"]="755:0:0"
        ["usr/share/man/man1"]="755:0:0"
        ["usr/share/man/man2"]="755:0:0"
        ["usr/share/man/man3"]="755:0:0"
        ["usr/share/man/man4"]="755:0:0"
        ["usr/share/man/man5"]="755:0:0"
        ["usr/share/man/man6"]="755:0:0"
        ["usr/share/man/man7"]="755:0:0"
        ["usr/share/man/man8"]="755:0:0"
        ["usr/share/misc"]="755:0:0"
        ["usr/share/pixmaps"]="755:0:0"
        ["usr/share/terminfo"]="755:0:0"
        ["usr/share/zoneinfo"]="755:0:0"
        ["usr/src"]="755:0:0"
        ["var/cache"]="755:0:0"
        ["var/lib/color"]="755:0:0"
        ["var/lib/misc"]="755:0:0"
        ["var/lib/locate"]="755:0:0"
        ["var/local"]="755:0:0"
        ["var/log"]="755:0:0"
        ["var/mail"]="755:0:0"
        ["var/opt"]="755:0:0"
        ["var/spool"]="755:0:0"
        ["var/tmp"]="1777:0:0"
    )

    symlinks=(
        ["bin"]="usr/bin"
        ["etc/mtab"]="../proc/self/mounts"
        ["lib"]="usr/lib"
        ["lib64"]="usr/lib64"
        ["sbin"]="usr/sbin"
        ["var/lock"]="../run/lock"
        ["var/run"]="../run"
    )

    files=(
        ["etc/fstab"]="fstab:644:0:0"
        ["etc/future-release"]="future-release:644:0:0"
        ["etc/group"]="group:644:0:0"
        ["etc/hostname"]="hostname:644:0:0"
        ["etc/hosts"]="hosts:644:0:0"
        ["etc/issue"]="issue:644:0:0"
        ["etc/locale.conf"]="locale.conf:644:0:0"
        ["etc/passwd"]="passwd:644:0:0"
        ["etc/os-release"]="os-release:644:0:0"
        ["usr/share/pixmaps/futurelinux-logo.png"]="futurelinux-logo.png:644:0:0"
        ["usr/share/pixmaps/futurelinux-logo.svg"]="futurelinux-logo.svg:644:0:0"
        ["usr/share/pixmaps/futurelinux-logo-text.svg"]="futurelinux-logo-text.svg:644:0:0"
        ["usr/share/pixmaps/futurelinux-logo-text-dark.svg"]="futurelinux-logo-text-dark.svg:644:0:0"
        ["var/log/btmp"]="btmp:600:0:0"
        ["var/log/lastlog"]="lastlog:664:0:13"
        ["var/log/faillog"]="faillog:644:0:0"
        ["var/log/wtmp"]="wtmp:644:0:0"
    )

    cd ${pkgdir}

    for dir in ${!directories[@]}; do
        mode=$(cut -f 1 -d ':' <<< ${directories[${dir}]})
        user=$(cut -f 2 -d ':' <<< ${directories[${dir}]})
        group=$(cut -f 3 -d ':' <<< ${directories[${dir}]})

        install -vdm ${mode} -o ${user} -g ${group} ${dir}
    done

    for link in ${!symlinks[@]}; do
        ln -sv ${symlinks[${link}]} ${link}
    done

    for target_file in ${!files[@]}; do
        source_file=$(cut -f 1 -d ':' <<< ${files[${target_file}]})
        mode=$(cut -f 2 -d ':' <<< ${files[${target_file}]})
        user=$(cut -f 3 -d ':' <<< ${files[${target_file}]})
        group=$(cut -f 4 -d ':' <<< ${files[${target_file}]})

        install -vDm ${mode} -o ${user} -g ${group} ${srcdir}/${source_file} ${target_file}
    done

}
